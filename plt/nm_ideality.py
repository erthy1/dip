import numpy as np
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import settings as s

#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d692 = [
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.52_NMIV_692_12#3_iv2p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__13.55_NMIV_692_12#3_iv2n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.40_NMIV_692_12#4_iv1p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.43_NMIV_692_12#4_iv1n.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.56_NMIV_692_12#4_iv3p.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #692 FIB\\2017_07_25__14.58_NMIV_692_12#4_iv3n.txt',
]

fig = plt.figure(num=None, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
#fig = plt.figure()

# --- d692
d = np.loadtxt(d692[3])
half = int(len(d[:,0])/2)
x, y = -d[:,0][:half], -d[:,1][:half]
#plt.loglog(x, y, 'v', color="#ffa500", label=s.label_fib)

# --- 6 to 18 is exp()
def ex(x, alpha, c):
    return np.exp(alpha*x)*c

def te(U, n_ideality):
    A = 1.20173e6  # [A m-2 K-2]
    k_B = 1.38064852e-23  # [J K-1]
    q = 1.60217646e-19
    SBH = 0.5  # eV
    T = 20 + 273.5
    return A*T**2*np.exp((-q*SBH)/(k_B*T))*np.exp((q*U)/(n_ideality*k_B*T)) * C

def de(U, n):
    I_s = 0.5e-11
    k_B = 1.38064852e-23  # [J K-1]
    q = 1.60217646e-19
    T = 273.5 + 20
    V_T = k_B*T/q

    return I_s*(np.exp(U/(n*V_T)) -1)

x_exp = x[y>1e-12]  #[6:18]
y_exp = y[y>1e-12]  #[6:18]
popt, pcov = curve_fit(de, x_exp, y_exp, bounds=(10, 60))
#print(popt)  # exp coefficient
x_exp_cont = np.arange(x_exp[0], x_exp[-1], 0.001)
plt.loglog(x_exp_cont, de(x_exp_cont, *popt), color="#1e76b3")
print(popt[0])
plt.loglog(x_exp, y_exp, 'v', color="#ffa500", label=s.label_fib)
#plt.loglog(x_exp_cont, de(x_exp_cont, 55, ), color="#1e76b3")
#plt.annotate('$I\\sim e^{\\alpha U}$', xy=(4, 6e-11), xytext=(4, 6e-11), color='#1e76b3')

# --- cosmetics
#plt.title('n-ZnO NR / p-GaN')
plt.xlabel(s.label('U'))
plt.ylabel(s.label('I'))
plt.legend(loc='upper left')

plt.tight_layout()
plt.show()

# # --- d527
# d = np.loadtxt(dn[2])
# half = int(len(d[:,0])/2)
# x,y = -d[:,0][:half], -d[:,1][:half]
# plt.loglog(x, y, 'o', color="#ffa500", label=s.label_nofib)
#
# # --- 0 to 10 is exp()
# def ex(x, a, b, c):
#     return a*np.exp(b*x)+c
# def expfit(x, y):
#     lx, ly = np.log(x), np.log(y)
#     popt, pcov = curve_fit(ex, lx, ly)
#     lx_dense = np.arange(lx[0], lx[-1], 0.001)
#     fit_vals = ex(lx_dense, *popt)
#     return lx_dense, fit_vals
# lx, fit_vals = expfit(x[2:10], y[2:10])
# plt.loglog(np.exp(lx), np.exp(fit_vals))
# # plt.loglog(x_exp_cont, ex(x_exp_cont, popt[0], popt[1]), color="#1e76b3")
# plt.annotate('$I\\sim e^{\\alpha U}$', xy=(4, 6e-11), xytext=(0.5, 2e-11), color='#1e76b3')
#
# # --- 10 to end is V^n
# def fit(x, y):
#     logx = np.log(x)
#     logy = np.log(y)
#     coeffs = np.polyfit(logx, logy, deg=1)
#     poly = np.poly1d(coeffs)
#     return poly
#
# x_pow = x[10:18]
# y_pow = y[10:18]
# poly1 = fit(x_pow, y_pow)
# print(poly1)  # power coefficient
# x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
# yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
# plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
# plt.annotate('$I\\sim U^n$', xy=(2.5, 8e-8), xytext=(1.2, 8e-9), color='#1e76b3')
#
# x_pow = x[20:-20]
# y_pow = y[20:-20]
# poly1 = fit(x_pow, y_pow)
# print(poly1)  # power coefficient
# x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
# yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
# plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
# plt.annotate('$I\\sim U^2$', xy=(2.5, 8e-8), xytext=(3.0, 2.5e-7), color='#1e76b3')
#
# plt.annotate('PF', xy=(2.5, 8e-8), xytext=(7.7, 1.2e-6), color='#1e76b3')
#
# ax = plt.gca()
# ax.annotate("", xy=(1.8, 3.9e-10), xytext=(1.35, 1.02e-9), arrowprops=dict(arrowstyle="-", linestyle="--"))
# ax.annotate("", xy=(3.5, 1.2e-8), xytext=(2.7, 5e-8), arrowprops=dict(arrowstyle="-", linestyle="--"))
# ax.annotate("", xy=(7.6, 1.4e-7), xytext=(6.9, 8e-7), arrowprops=dict(arrowstyle="-", linestyle="--"))
#
# ax.annotate("", xy=(10.1, 1.1e-10), xytext=(7.8, 4.3e-10), arrowprops=dict(arrowstyle="-", linestyle="--"))
#
# plt.ylim([1e-13, 5e-6])
#
# plt.legend(loc='upper left')
#
#
# # ---
# plt.tight_layout()
#
# plt.savefig('out/nm_fits.pdf')
# plt.savefig('out/nm_fits.pgf')
# s.fix_pgf_lyx('out/nm_fits.pgf')
#plt.show()
