import numpy as np

import matplotlib as mpl
import settings as s
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)

import matplotlib.pyplot as plt
import csv
from openpyxl import Workbook
import os
import errno


def make_way(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def load_data(file_name):
    data = np.loadtxt(file_name).T
    half = int(len(data[0]) / 2)
    x1, y1 = data[4][half:], data[0][half:] * 1e9
    x2, y2 = data[4][:half], data[0][:half] * 1e9
    return x1, y1, x2, y2


def make_plot(folder, file_names, colors, title, pid, xlim, ylim):
    fig = plt.figure(num=1, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
    #plt.clf()

    for i in range(len(file_names)):
        x1, y1, x2, y2 = load_data(folder + file_names[i] + '.txt')

        if folder=="data/jpk/side/":
            legend_label = 'side facet'
            if i:
                legend_label = '_nolegend_'
            plt.plot(x1, y1, color=s.blue, lw=1, label=legend_label)
        else:
            legend_label = 'top facet'
            if i:
                legend_label = '_nolegend_'
            plt.plot(x1, y1, color=s.red, lw=1, label=legend_label)
        #plt.plot(x2, y2, color=colors[i], lw=1)

    plt.xlabel(s.label('U'))
    plt.ylabel(s.label('I', prefix='n'))
    plt.xlim(xlim)
    plt.ylim(ylim)

    # handles = []
    # handles.append(mpl.patches.Patch(color=yellow, label="side facet"))
    # handles.append(mpl.patches.Patch(color=blue, label="top facet"))
    # leg = plt.legend(handles=handles, loc='best')
    # for legobj in leg.legendHandles:
    #     legobj.set_linewidth(1.0)
    plt.legend(loc='best')
    #plt.title(title)
    plt.tight_layout()
    # plt.show()
    if folder!="data/jpk/side/":
        plt.savefig('out/jpk_combined' + '.pdf', dpi=300)
        plt.savefig('out/jpk_combined' + '.pgf', dpi=300)
        s.fix_pgf_lyx('out/jpk_combined' + '.pgf')


colors = ['red', 'blue', 'green', 'magenta', 'red']

folder2 = 'data/jpk/side/'
title2 = 'b) Side facet of nanorod'

file_names = [
    'voltage-spectroscopy-2017.02.23-16.48.04.591',
    'voltage-spectroscopy-2017.02.23-16.47.54.876',
    'voltage-spectroscopy-2017.02.23-16.48.01.353',
    'voltage-spectroscopy-2017.02.23-16.47.48.400',
]

make_plot(folder2, file_names, colors, title2, 'out/edge', [-8, 4.2], [-126, 126])


folder1 = 'data/jpk/top/'
title1 = 'a) Top facet of nanorod'

file_names = [
    #'voltage-spectroscopy-2017.02.23-16.50.58.885',
    'voltage-spectroscopy-2017.02.23-16.51.02.123',
    'voltage-spectroscopy-2017.02.23-16.51.05.361',
    #'voltage-spectroscopy-2017.02.23-16.51.08.600',
]

make_plot(folder1, file_names, colors, title1, 'out/top', [-8, 4.2], [-126, 126])
