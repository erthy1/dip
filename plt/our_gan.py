# -*- coding: utf-8 -*-

import numpy as np
from io import StringIO   # StringIO behaves like a file object
import glob
import os
import errno
import settings as s

import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt


def get_separate_measurements(datafile):
    """
    Takes a file, separates measurements and returns a list of faked files.
    """
    clear_utf(datafile)
    with open(datafile, 'r', encoding='utf-8') as myfile:
        data = myfile.read()
        data = data.split('\n\n')
        sweep_id = data[0].split('\n')[0].replace("=", "").replace("#  Sweep ID: ", "")
    datafiles = []
    for datastring in data:
        datafiles.append(StringIO(datastring))

    return datafiles, sweep_id


def clear_utf(file):
    with open(file, 'r', encoding='utf-8') as myfile:
        data = myfile.read()
    if 'č' in data or 'í' in data or 'á' in data or 'ů' in data:
        data = data.replace('č', 'c').replace('á', 'a').replace('í', 'i').replace('ů', 'u')
        with open(file, 'w', encoding='utf-8') as myfile:
            myfile.write(data)


def get_datafiles(directory):
    os.chdir(directory)
    files = []
    for file in glob.glob("*.txt"):
        if "_info.txt" not in file:
            files.append(file)
    # return files[0:25]
    return files


def make_way(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


colorlist = ['#FF0000', '#F2000D', '#E6001A', '#D90026', '#CC0033', '#BF0040',
             '#B2004C', '#A60059', '#990066', '#8C0073', '#800080', '#73008C', '#660099',
             '#5900A6', '#4D00B2', '#4000BF', '#3300CC', '#2600D9', '#1900E6', '#0D00F2',
             '#0000FF']


def compare(datafiles, savename='figure.png'):
    fig = plt.figure(num=1, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
    plt.clf()
    #colors = ['#F6BC25', '#FA9D3D', '#FC8152', '#FF6666']
    #colors = ['#C6031C', '#5E3C9B', '#8197C8', '#0572B3']
    colors = [s.blue, s.altpurple1, s.altpurple2, s.red]#, '#FF6666']
    # colors = ['#E8AE34', '#DA9A46', '#D08B54', '#BF7469']
    # colors = ['#4a9ee3', '#939fbc', '#d2a276', '#ffa500']

    # colors = ['#93D841', '#29AE7F', '#287D8E', '#463781']
    #labels = ['0 V', '2 V', '4 V', '6 V']
    labels = ['~\,50 nN', '100 nN', '150 nN', '200 nN']
    handles = []
    for i in range(len(datafiles)):
        dfiles, sweep_id = get_separate_measurements(datafiles[i])

        title = datafiles[i]

        #num_plots = len(dfiles[0:-1])
        #plt.gca().set_prop_cycle(plt.cycler('color', plt.cm.winter(np.linspace(0, 1, num_plots))))

        #to_avg = []
        for dfile in dfiles[3:4]:
            try:
                I, V = np.loadtxt(dfile, comments='#', unpack=True)
            except:
                print(title)
                raise
            I_corr = I * 1e-3
            #plt.scatter(V, I_corr, alpha=0.4, color = colors[i], s=1)
            plt.plot(V, I_corr * 1e9, alpha=1, color=colors[i])
        handles.append(mpl.patches.Patch(
            color=colors[i], label=labels[i]))
        #folder_title = '/'.join(folder.split('\\')[-1:])
    # plt.title(title.split('/')[1])
    plt.xlabel(s.label('U'))
    plt.ylabel(s.label('I', prefix='n'))
    plt.ylim([-10, 10])
    plt.xlim([-6.5, 8.5])

    #plt.tight_layout()
    plt.legend(handles=handles, loc='best')
    #plt.show()
    #plt.savefig('out/poster_pritlak.png', dpi=300)
    plt.annotate("", xy=(2.6, 7.5), xytext=(8, 7.5), arrowprops=dict(arrowstyle="->", linestyle='--'))

    plt.tight_layout()
    plt.savefig('out/our_gan' + '.pdf', dpi=300)
    plt.savefig('out/our_gan' + '.pgf', dpi=300)
    s.fix_pgf_lyx('out/our_gan' + '.pgf')

    plt.xlabel(s.label_cs('U'))
    plt.ylabel(s.label_cs('I', prefix='n'))
    plt.savefig('C:\Syncs\Main\Skola\FJFI_2018_LS\sdp\plt\\our_gan' + '.pdf', dpi=300)

    # make_way('color')
    #plt.savefig('color/'+title.replace('.txt', '_color.png'))


path = 'C:\\Syncs\\Main\\Skola\\UFE\\mereni\\'
#folders = ['mereni_1', 'mereni_2', 'mereni_3', 'mereni_4', 'mereni_5', 'mereni_6', 'honzova_mereni', 'honzovo_mereni_var_svetlo']
#
# for folder in folders:
#    zpracuj_slozku(path+'\\'+folder)

# Vliv pritlaku
# files_pritlak_1 = ['mereni_1/sweep_2017-01-19_15-43-41_FB_0V.txt', 'mereni_1/sweep_2017-01-19_15-51-31_FB_2V.txt',
#                   'mereni_1/sweep_2017-01-19_15-59-35_FB_4V.txt', 'mereni_1/sweep_2017-01-19_16-07-15_FB_6V.txt',]
# files_pritlak_2_1 = ['mereni_2/sweep_2017-01-19_16-57-38_FB_0V_2.txt', 'mereni_2/sweep_2017-01-19_17-04-41_FB_2V_2.txt',
#                   'mereni_2/sweep_2017-01-19_17-12-46_FB_4V_2.txt', 'mereni_2/sweep_2017-01-19_17-22-45_FB_6V_2.txt']
# files_pritlak_2_2 = ['mereni_2/sweep_2017-01-19_16-54-27_FB_0V.txt', 'mereni_2/sweep_2017-01-19_17-01-24_FB_2V.txt',
#                     'mereni_2/sweep_2017-01-19_17-09-06_FB_4V.txt', 'mereni_2/sweep_2017-01-19_17-17-36_FB_6V.txt']
# files_pritlak_3_2 = ['mereni_3/sweep_2017-01-19_17-59-10_FB_0V_2.txt', 'mereni_3/sweep_2017-01-19_18-07-10_FB_2V_2.txt',
#                     'mereni_3/sweep_2017-01-19_18-14-11_FB_4V.txt', 'mereni_3/sweep_2017-01-19_18-20-27_FB_6V.txt']
# files_pritlak_4_1 = ['mereni_4/sweep_2017-01-20_10-09-43_FB_0V.txt', 'mereni_4/sweep_2017-01-20_10-20-13_FB_2V.txt',
#                     'mereni_4/sweep_2017-01-20_10-29-56_FB_4V.txt', 'mereni_4/sweep_2017-01-20_10-38-57_FB_6V.txt']
files_pritlak_4_rev = ['mereni_4/sweep_2017-01-20_10-38-57_FB_6V.txt', 'mereni_4/sweep_2017-01-20_10-48-16_FB_4V_3.txt',
                       'mereni_4/sweep_2017-01-20_10-58-25_FB_2V_3.txt', 'mereni_4/sweep_2017-01-20_11-05-47_FB_0V_3.txt']
# files_pritlak_5_1 = ['mereni_5/sweep_2017-01-20_11-46-54_FB_0V.txt', 'mereni_5/sweep_2017-01-20_12-00-53_FB_2V.txt',
#                     'mereni_5/sweep_2017-01-20_13-06-36_FB_4V.txt', 'mereni_5/sweep_2017-01-20_13-14-41_FB_6V.txt', ]
# files_pritlak_5_2 = ['mereni_5/sweep_2017-01-20_11-50-51_FB_0V_2.txt', 'mereni_5/sweep_2017-01-20_12-05-50_FB_2V_2.txt',
#                     'mereni_5/sweep_2017-01-20_13-10-19_FB_4V_2.txt', 'mereni_5/sweep_2017-01-20_13-17-51_FB_6V_2.txt', ]
# files_pritlak_5_rev = ['mereni_5/sweep_2017-01-20_13-17-51_FB_6V_2.txt', 'mereni_5/sweep_2017-01-20_13-25-25_FB_4V_3.txt',
#                     'mereni_5/sweep_2017-01-20_13-35-08_FB_2V_3.txt', 'mereni_5/sweep_2017-01-20_13-48-39_FB_0V_3.txt', ]
# files_pritlak_honza = ['honzova_mereni/sweep_2017-01-19_14-07-59.txt', 'honzova_mereni/sweep_2017-01-19_14-34-21_FB_2V.txt',
#                       'honzova_mereni/sweep_2017-01-19_14-44-26_FB_4V.txt', 'honzova_mereni/sweep_2017-01-19_14-58-23_FB_6V.txt', ]
# files_pritlak_honza_svetlo = ['honzova_mereni/sweep_2017-01-19_14-12-12_svetlo.txt', 'honzova_mereni/sweep_2017-01-19_14-19-49_FB_2V_svetlo.txt',
#                       'honzova_mereni/sweep_2017-01-19_14-41-14_FB_4V.txt', 'honzova_mereni/sweep_2017-01-19_14-54-58_FB_6V_svetlo.txt', ]

# Vliv pritlaku
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_1], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_1.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_2_1], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_2_1.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_2_2], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_2_2.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_3_2], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_3_2.png')
# compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_4_1], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_4_1.png')
compare([path+'2017_01_19-20_vsechno_ZnO_roztrizeno/' + file for file in files_pritlak_4_rev[::-1]],
        savename='out/' + 'pritlak_4_rev.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_5_1], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_5_1.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_5_2], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_5_2.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_5_rev[::-1]], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'pritlak_5_rev.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_honza], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'honzova_mereni.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_pritlak_honza_svetlo], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'honzova_mereni_svetlo.png')

# Vliv svetla
# files_svetlo_1 = ['mereni_1/sweep_2017-01-19_15-36-19_FB_0V_svetlo.txt', 'mereni_1/sweep_2017-01-19_15-39-58_FB_0V_svetlo_2.txt',
#                  'mereni_1/sweep_2017-01-19_15-43-41_FB_0V.txt', 'mereni_1/sweep_2017-01-19_15-47-34_FB_0V_2.txt', ]
# files_svetlo_2 = ['mereni_2/sweep_2017-01-19_16-44-35_FB_0V_svetlo.txt', 'mereni_2/sweep_2017-01-19_16-48-44_FB_0V_svetlo_2.txt',
#                  'mereni_2/sweep_2017-01-19_16-54-27_FB_0V.txt', 'mereni_2/sweep_2017-01-19_16-57-38_FB_0V_2.txt']
# files_svetlo_3 = ['mereni_3/sweep_2017-01-19_17-48-33_FB_0V_svetlo_3.txt', 'mereni_3/sweep_2017-01-19_17-51-59_FB_0V_svetlo_4.txt',
#                  'mereni_3/sweep_2017-01-19_17-55-35_FB_0V.txt', 'mereni_3/sweep_2017-01-19_17-59-10_FB_0V_2.txt', ]

# Vliv svetla
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_svetlo_1], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'svetlo_mereni_1.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_svetlo_2], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'svetlo_mereni_2.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_svetlo_3], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'svetlo_mereni_3.png')

# Vliv casu
# files_time_1 = ['mereni_1/sweep_2017-01-19_15-36-19_FB_0V_svetlo.txt', 'mereni_1/sweep_2017-01-19_15-39-58_FB_0V_svetlo_2.txt',
#                'mereni_1/sweep_2017-01-19_15-43-41_FB_0V.txt', 'mereni_1/sweep_2017-01-19_15-47-34_FB_0V_2.txt', ]
# files_time_3 = ['mereni_3/sweep_2017-01-19_17-42-04_FB_0V_svetlo.txt', 'mereni_3/sweep_2017-01-19_17-45-18_FB_0V_svetlo_2.txt',
#                'mereni_3/sweep_2017-01-19_17-48-33_FB_0V_svetlo_3.txt', 'mereni_3/sweep_2017-01-19_17-51-59_FB_0V_svetlo_4.txt', ]
# files_time_4 = ['mereni_4/sweep_2017-01-20_10-02-24_FB_0V_svetlo.txt', 'mereni_4/sweep_2017-01-20_10-06-01_FB_0V_svetlo_2.txt',
#                'mereni_4/sweep_2017-01-20_10-09-43_FB_0V.txt', 'mereni_4/sweep_2017-01-20_10-13-19_FB_0V_2.txt', ]

# Vliv casu
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_time_1], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'cas_mereni_1.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_time_3], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'cas_mereni_3.png')
#compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in files_time_4], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'cas_mereni_4.png')

# pritlak_0 = ['mereni_2/sweep_2017-01-19_16-57-38_FB_0V_2.txt', 'mereni_4/sweep_2017-01-20_10-13-19_FB_0V_2.txt', 'mereni_5/sweep_2017-01-20_11-50-51_FB_0V_2.txt', 'mereni_4/sweep_2017-01-20_11-10-19_FB_0V_4.txt']
# pritlak_2 = ['mereni_2/sweep_2017-01-19_17-04-41_FB_2V_2.txt', 'mereni_4/sweep_2017-01-20_10-23-52_FB_2V_2.txt', 'mereni_5/sweep_2017-01-20_12-05-50_FB_2V_2.txt', 'mereni_4/sweep_2017-01-20_10-58-25_FB_2V_3.txt', ]
# pritlak_4 = ['mereni_2/sweep_2017-01-19_17-12-46_FB_4V_2.txt', 'mereni_4/sweep_2017-01-20_10-33-37_FB_4V_2.txt', 'mereni_5/sweep_2017-01-20_13-06-36_FB_4V.txt', 'mereni_4/sweep_2017-01-20_10-48-16_FB_4V_3.txt', ]
# pritlak_6 = ['mereni_2/sweep_2017-01-19_17-22-45_FB_6V_2.txt', 'mereni_4/sweep_2017-01-20_10-42-13_FB_6V_2.txt', 'mereni_5/sweep_2017-01-20_13-22-02_FB_6V_3.txt', 'mereni_4/sweep_2017-01-20_10-38-57_FB_6V.txt', ]
#
# compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in pritlak_0], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'all_pritlak_0.png')
# compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in pritlak_2], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'all_pritlak_2.png')
# compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in pritlak_4], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'all_pritlak_4.png')
# compare(['20170119-20_vsechno_ZnO_roztrizeno/'+ file for file in pritlak_6], savename='20170119-20_vsechno_ZnO_roztrizeno/'+'all_pritlak_6.png')
