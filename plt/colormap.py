import matplotlib as mpl # in python
import numpy as np

def generate_cmap(filename):
    cmap = []
    with open(filename, 'r') as f:
        for line in f:
            rgb = line.split()
            rgbt = [int(x) for x in rgb]
            cmap.append(rgbt)
    return np.array(cmap)

C = generate_cmap('colormap.txt')
cm = mpl.colors.ListedColormap(C/255.0)

cm2 = [tuple([i/255 for i in x]) for x in C]
