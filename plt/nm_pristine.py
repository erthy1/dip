import numpy as np
from scipy.optimize import curve_fit
import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt
import settings as s

def half(d, first=True):
    half = int(len(d[:,0])/2)
    if first:
        x, y = -d[:,0][:half], -d[:,1][:half]
    else:
        x, y = -d[:,0][half:], -d[:,1][half:]
    return x, y

def unp(d):
    d = d[np.abs(d[:,0])>0.6]
    x, y = -d[:,0], -d[:,1]
    return x, y


#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d527 = [
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.44_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.45_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.46_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.47_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.48_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.49_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.50_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.51_NMIV_test_sweep.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_02_05__11.52_NMIV_test_sweep.txt',
]

fig = plt.figure(num=None, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
#fig = plt.figure()

# --- d692
# d = np.loadtxt(d527[0])
# #x, y = half(d, first=False)
# x, y = unp(d)
# plt.plot(x, y, '-', color="#c33c54", label="#1")
#
# d = np.loadtxt(d527[3])
# #x, y = half(d, first=False)
# x, y = unp(d)
# plt.plot(x, y, '-', color="#ff8c42", label="#1")
# #ffa500
# --- noFIB?
d = np.loadtxt(d527[4])
#x, y = half(d)
x, y = unp(d)
plt.plot(x, y, '-', color=s.blue, label="\#1")


d = np.loadtxt(d527[6])
#x, y = half(d, first=False)
x, y = unp(d)
plt.plot(x, y, '-', color=s.purple, label="\#2")

d = np.loadtxt(d527[8])
#x, y = half(d, first=False)
x, y = unp(d)
plt.plot(x, y, '-', color=s.red, label="\#3")

# #1e76b3
ax = plt.gca()
ax.set_yscale("log")
ax.set_xscale("log")
# d = np.loadtxt(d527[3])
# #x, y = half(d, first=True)
# x, y = unp(d)
# plt.plot(x, y, 'v', color="#1e76b3")


#plt.xlim([0.6,11])
#plt.ylim([1e-12, 1e-5])

plt.xlabel(s.label('U'))
plt.ylabel(s.label('I'))

lang="cs"
if lang=="cs":
    bubble = "před desorpcí"
else:
    bubble = "before desorption"

plt.text(0.025, 0.93, bubble, size=12, rotation=0.,
         ha="left", va="center",
         bbox=dict(boxstyle="round",
                   ec=(0.5, 0.5, 0.5),#(0.5, 1, 0.5),
                   fc=(0.9, 0.9, 0.9),#(0.8, 1, 0.8),
                   ),
         transform = ax.transAxes,
         )

ax.annotate("", xy=(3.3, 2.4e-8), xytext=(2.48, 8.49e-9), arrowprops=dict(arrowstyle="->"))
ax.annotate("", xy=(2.6, 1.1e-9), xytext=(3.39, 2.6e-9), arrowprops=dict(arrowstyle="->"))

#ax.annotate("", xy=(6.38, 2.1e-8), xytext=(4.79, 1.9e-6), arrowprops=dict(arrowstyle="->", linestyle='--'))

#plt.text(0.65, 1.5e-6, 'After desorption', fontsize=12, position)

#plt.title('After desorption')
plt.legend(loc='lower right')

# ---
plt.tight_layout()
plt.savefig('out/nm_pristine.pdf')
plt.savefig('out/nm_pristine.pgf')
s.fix_pgf_lyx('out/nm_pristine.pgf')

plt.xlabel(s.label_cs('U'))
plt.ylabel(s.label_cs('I'))
plt.savefig('C:\Syncs\Main\Skola\FJFI_2018_LS\sdp\plt\\nm_pristine' + '.pdf', dpi=300)
