import numpy as np
from scipy.optimize import curve_fit
import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt
import settings as s

def half(d, first=True):
    half = int(len(d[:,0])/2)
    if first:
        x, y = -d[:,0][:half], -d[:,1][:half]
    else:
        x, y = -d[:,0][half:], -d[:,1][half:]
    return x, y

def unp(d):
    d = d[np.abs(d[:,0])>0.5]
    x, y = -d[:,0], -d[:,1]
    return x, y


#data = 'DATA\\nZnO_sNR on n-GaN #556 FIB\\2017_08_04__15.46_NMIV_p13iv1n.txt'
d527 = [
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.21_NMIV_527_nr3_neg.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.23_NMIV_527_nr3_pos.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.24_NMIV_527_nr3_neg.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.25_NMIV_527_nr3_pos.txt',
    'data\\stanislav_epfl\\DATA\\nZnO_sNR on pGaN #527 no-FIB\\2018_01_22__16.27_NMIV_527_nr3_neg.txt',
]

fig = plt.figure(num=None, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
#fig = plt.figure()

# --- d692
d = np.loadtxt(d527[0])
#x, y = half(d, first=False)
x, y = unp(d)
plt.plot(x, y, '-', color=s.blue, label="\#1")

d = np.loadtxt(d527[2])
#x, y = half(d, first=False)
x, y = unp(d)
plt.plot(x, y, '-', color=s.purple, label="\#2")
# #ffa500
# --- noFIB?
d = np.loadtxt(d527[4])
#x, y = half(d)
x, y = unp(d)
plt.plot(x, y, '-', color=s.red, label="\#3")
# #1e76b3
ax = plt.gca()
ax.set_yscale("log")
ax.set_xscale("log")
# d = np.loadtxt(d527[3])
# #x, y = half(d, first=True)
# x, y = unp(d)
# plt.plot(x, y, 'v', color="#1e76b3")


plt.xlim([0.6,11])
#plt.ylim([1e-12, 1e-5])

plt.xlabel(s.label('U'))
plt.ylabel(s.label('I'))
lang="cs"
if lang=="cs":
    bubble = "po desorpci"
else:
    bubble = "after desorption"
plt.text(0.025, 0.93, bubble, size=12, rotation=0.,
         ha="left", va="center",
         bbox=dict(boxstyle="round",
                   fc=(0.9, 0.9, 0.9),#(50/256, 193/256, 256/256),#(1., 0.5, 0.5),
                   ec=(0.5, 0.5, 0.5),#(5/256, 113/256, 176/256),#(5/256, 3/256, 66/256),#(1., 0.8, 0.8),
                   ),
         transform = ax.transAxes,
         )

ax.annotate("", xy=(3.0, 4.3e-7), xytext=(2.36, 8.8e-8), arrowprops=dict(arrowstyle="->"))
ax.annotate("", xy=(2.6, 1.98e-8), xytext=(3.32, 5.9e-8), arrowprops=dict(arrowstyle="->"))

ax.annotate("", xy=(6.38, 2.1e-8), xytext=(4.79, 1.9e-6), arrowprops=dict(arrowstyle="->", linestyle='--'))

#plt.text(0.65, 1.5e-6, 'After desorption', fontsize=12, position)

#plt.title('After desorption')
plt.legend(loc='lower right')

# ---
plt.tight_layout()
plt.savefig('out/nm_desorption.pdf')
plt.savefig('out/nm_desorption.pgf')
s.fix_pgf_lyx('out/nm_desorption.pgf')

plt.xlabel(s.label_cs('U'))
plt.ylabel(s.label_cs('I'))
plt.savefig('C:\Syncs\Main\Skola\FJFI_2018_LS\sdp\plt\\nm_desorption' + '.pdf', dpi=300)

#plt.show()

# # --- 6 to 18 is exp()
# def ex(x, alpha, c):
#     return np.exp(alpha*x)*c
# x_exp = x[6:18]
# y_exp = y[6:18]
# popt, pcov = curve_fit(ex, x_exp, y_exp)
# #print(popt)  # exp coefficient
# x_exp_cont = np.arange(x_exp[0], x_exp[-1], 0.001)
# plt.loglog(x_exp_cont, ex(x_exp_cont, popt[0], popt[1]), color="#1e76b3")
# plt.annotate('$I\\approx e^{\\alpha V}$', xy=(4, 6e-11), xytext=(4, 6e-11), color='#1e76b3')
#
# # # --- 19 to end is V^n
# def fit(x, y):
#     logx = np.log(x)
#     logy = np.log(y)
#     coeffs = np.polyfit(logx, logy, deg=1)
#     poly = np.poly1d(coeffs)
#     return poly
# x_pow = x[19:]
# y_pow = y[19:]
# poly1 = fit(x_pow, y_pow)
# #print(poly1)  # power coefficient
# x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
# yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
# plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
# plt.annotate('$I\\approx V^n$', xy=(8.7, 6e-9), xytext=(8.7, 6e-9), color='#1e76b3')
#
# # --- cosmetics
# plt.title('n-ZnO NR / p-GaN')
# plt.xlabel('Voltage (V)')
# plt.ylabel('Current (A)')
# plt.legend(loc='upper left')
# plt.xlim([0.2, 25])
# plt.ylim([5e-13, 1e-7])
#
# plt.tight_layout()
# #plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\pn01.png')
#
# # --- 3e-12 noise + annotation
# #plt.axhline(y=3e-12, color='r', linestyle='--')
# #plt.annotate('noise level', xy=(7, 2e-12), xytext=(10, 1.7e-12), color='red')
#
# plt.tight_layout()
# #plt.savefig('C:\Syncs\Main\Responsible\phd\epfl_roke\pre2\plt\pn02.png')
#
#
#
# # --- d527
# d = np.loadtxt(d527[3])
# half = int(len(d[:,0])/2)
# x,y = -d[:,0][:half], -d[:,1][:half]
# plt.loglog(x, y, 'o', color="#ffa500", label='without FIB')
#
# # --- 0 to 10 is exp()
# def ex(x, a, b, c):
#     return a*np.exp(b*x)+c
# def expfit(x, y):
#     lx, ly = np.log(x), np.log(y)
#     popt, pcov = curve_fit(ex, lx, ly)
#     lx_dense = np.arange(lx[0], lx[-1], 0.001)
#     fit_vals = ex(lx_dense, *popt)
#     return lx_dense, fit_vals
# lx, fit_vals = expfit(x[2:9], y[2:9])
# plt.loglog(np.exp(lx), np.exp(fit_vals))
# # plt.loglog(x_exp_cont, ex(x_exp_cont, popt[0], popt[1]), color="#1e76b3")
# plt.annotate('$I\\approx e^{\\alpha V}$', xy=(4, 6e-11), xytext=(0.7, 1.5e-10), color='#1e76b3')
#
# # --- 10 to end is V^n
# def fit(x, y):
#     logx = np.log(x)
#     logy = np.log(y)
#     coeffs = np.polyfit(logx, logy, deg=1)
#     poly = np.poly1d(coeffs)
#     return poly
# x_pow = x[10:]
# y_pow = y[10:]
# poly1 = fit(x_pow, y_pow)
# print(poly1)  # power coefficient
# x_pow_cont = np.arange(x_pow[0], x_pow[-1], 0.001)
# yfit1 = lambda x: np.exp(poly1(np.log(x_pow_cont)))
# plt.loglog(x_pow_cont, yfit1(x_pow_cont), color="#1e76b3")
# plt.annotate('$I\\approx V^2$', xy=(2.5, 8e-8), xytext=(2.1, 5e-8), color='#1e76b3')
