import numpy as np
import matplotlib as mpl
import settings as s
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt

from os import listdir
from os.path import isfile, join, split
import sys

def get_jpk_index(filename):
    with open(filename, 'r') as myfile:
        index_str = myfile.read().split('\n')[8].split(' ')[2]
        index = int(index_str)
    return index

def flip(d):
    return -d

def half(data, pos=0):
    h = int(len(data)/2)
    if pos:
        return data[:h]
    else:
        return data[h:]

def strip_compliance(voltage, current):
    print(data.shape)
    index = np.where(np.abs(current)<100e-9)
    i = current[index]
    v = voltage[index]
    return i, v

folder = 'data/jpk_our/gan/'

full_filelist = [join(folder, f).replace('\\', '/') for f in listdir(folder) if (isfile(join(folder, f)) and (f[-4:]=='.txt'))]
print(full_filelist)

colorlist = ['#FF0000', '#F2000D', '#E6001A', '#D90026', '#CC0033', '#BF0040',
  '#B2004C', '#A60059', '#990066', '#8C0073', '#800080', '#73008C', '#660099',
  '#5900A6', '#4D00B2', '#4000BF', '#3300CC', '#2600D9', '#1900E6', '#0D00F2',
  '#0000FF']
colorlist = colorlist[3:-3]

# -2 čas
#  0 proud
#  4 napětí
fig = plt.figure(num=1, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')
plt.clf()

print(len(full_filelist))
#indexes = [4,5,6,7,8,9,12,15,19,22,25,29, -15, -10, -5]
#indexes = [4,5,7,9,10,11,12,16,20,24,28,32,36,40,44,48]
indexes = [10,11,12,16,20,24,28,32,36,40,44,45,46,47,48,49]
filelist = []
for i in indexes:
    filelist.append(full_filelist[i])

n = len(filelist)
colors = plt.cm.jet(np.linspace(0,1,n))

#filelist = full_filelist[4:-5] #[2:15]

m = len(filelist)
for i in range(0,m):
    print(filelist[i])
    data = np.loadtxt(filelist[i]).T
    I, U = strip_compliance(flip(half(data[2], pos=1)), flip(half(data[0], pos=1)))

    #plt.plot(U, I*1e9, color = colorlist[int((i/m)*len(colorlist))], lw=1, label = 'pos {}'.format(get_jpk_index(filelist[i])))
    plt.plot(U, I*1e9, color = s.cm2[int(i/m * len(s.cm2))], lw=1, label = 'pos {}'.format(get_jpk_index(filelist[i])))
plt.xlabel(s.label('U'))
plt.ylabel(s.label('I', prefix='n'))
#plt.legend(loc='best')

plt.xlim([-5.1,5.1])
plt.ylim([-105,105])
plt.tight_layout()
plt.annotate("", xy=(2, 75), xytext=(4.5, 75), arrowprops=dict(arrowstyle="->", linestyle='--'))
plt.savefig('out/jpk_gan' + '.pdf', dpi=300)
plt.savefig('out/jpk_gan' + '.pgf', dpi=300)
s.fix_pgf_lyx('out/jpk_gan' + '.pgf')
#plt.show()

plt.xlabel(s.label_cs('U'))
plt.ylabel(s.label_cs('I', prefix='n'))
plt.savefig('C:\Syncs\Main\Skola\FJFI_2018_LS\sdp\plt\jpk_gan' + '.pdf', dpi=300)
