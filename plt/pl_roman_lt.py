import numpy as np

# -*- coding: utf-8 -*-

import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)

import matplotlib.pyplot as plt
import settings as s

cmap = plt.cm.get_cmap('viridis')


yellow = "#ffa500"
blue = "#1e76b3"

colors = ['#FFB200', '#404040', '#5CE06F', '#3E78B2', '#FF724F']
colors = ['#FFB200', '#404040', '#7A5C61', '#5B9279', '#33658A']




#fig = plt.figure(num=None, figsize=(6, 3.5), dpi=100, facecolor='w', edgecolor='k')

data = np.loadtxt('data/pl_roman/05_detail.txt')
data = data[data[:,1]>0]

data[:,0] = 1.2398/data[:,0]*1e3

f, (ax, ax2) = plt.subplots(1, 2, sharey=True, figsize=s.figsize, dpi=s.dpi, facecolor='w', edgecolor='k')

# plot the same data on both axes
ax.semilogy(data[:,0], data[:,1]*1e2, label="bulk ZnO", color=s.blue)
ax2.semilogy(data[:,0], data[:,1]*1e2, label="bulk ZnO", color=s.blue)

# zoom-in / limit the view to different portions of the data
ax.set_xlim(367, 371.5)  # outliers only
ax2.set_xlim(371, 387)  # most of the dataB

ax.set_ylim(1e-2, 1e3)
ax2.set_ylim(1e-2, 1e3)

# hide the spines between ax and ax2
ax.spines['right'].set_visible(False)
ax2.spines['left'].set_visible(False)
ax2.yaxis.tick_right()
ax2.tick_params(labelright='off')  # don't put tick labels at the top
ax2.tick_params(
    axis='y',          # changes apply to the x-axis
    which='both',      # both major and minor ticks are affected
    right='off',      # ticks along the bottom edge are off
    left='off',         # ticks along the top edge are off
    #labelbottom='off' # labels along the bottom edge are off
    )
#ax2.yaxis.tick_right()
ax.get_yaxis().set_ticks([])

ax.set_ylabel(s.label('In'))
f.text(0.52, 0.04, s.label('lambda'), ha='center')

ax2.annotate('DAP', xy=(384.9,0.14))
ax2.annotate('BE-1LO', xy=(376,0.28))
ax2.annotate('TES', xy=(372.5,0.81))


ax.annotate('(Li)', xy=(369.3,120.29))
ax.annotate('(In,Na)', xy=(368.5,49.7))
ax.annotate('(Ga)', xy=(368.7,18))

d = .01  # how big to make the diagonal lines in axes coordinates
# arguments to pass to plot, just so we don't keep repeating them
kwargs = dict(transform=ax.transAxes, color='k', clip_on=False, lw=0.9)
ax.plot((1-d, 1+d), (1-d, 1+d), **kwargs)        # top-left diagonal
ax.plot((1 - d, 1 + d), (-d, +d), **kwargs)  # top-right diagonal

kwargs.update(transform=ax2.transAxes)  # switch to the bottom axes
ax2.plot((-d, +d), (1 - d, 1 + d), **kwargs)  # bottom-left diagonal
ax2.plot((-d,  +d), ( -d,  +d), **kwargs)  # bottom-right diagonal

#plt.show()

#plt.semilogy(data[:,0], data[:,1]*1e2, label="bulk ZnO", color=yellow)

# ax = plt.gca()
# ax.get_yaxis().set_ticks([])

#plt.xlim([320,339])
#plt.xlim([3.18,3.39])
#plt.xlabel('Wavelength (nm)')
#plt.ylabel('Intensity (a.u.)')
#plt.legend(loc='best')

plt.tight_layout()
plt.subplots_adjust(bottom=0.161)
plt.show()

plt.savefig('out/pl_roman_low.pdf')
plt.savefig('out/pl_roman_low.pgf')
s.fix_pgf_lyx('out/pl_roman_low.pgf')
