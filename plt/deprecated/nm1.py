import numpy as np
import matplotlib as mpl
mpl.use("pgf")
pgf_with_rc_fonts = {
    "font.family": "serif",
    "font.serif": [],                   # use latex default serif font
    "font.sans-serif": ["DejaVu Sans"], # use a specific sans-serif font
}
mpl.rcParams.update(pgf_with_rc_fonts)
import matplotlib.pyplot as plt

font = {'family': 'sans',
        'weight': 'normal',
        'size': 10,
        }

data = np.loadtxt('data/nm/2017_07_25__14.43_NMIV_692_12#4_iv1n.txt')
half = int(len(data)/2)
data_x = data[:,0][0:half]*-1
data_y = data[:,1][0:half]*-1
# Filtering values <0 for both x and y
x = data_x[(data_x>0)&(data_y>0)]
y = data_y[(data_x>0)&(data_y>0)]

def fit(x, y):
    logx = np.log(x)
    logy = np.log(y)
    coeffs = np.polyfit(logx, logy, deg=1)
    poly = np.poly1d(coeffs)
    return poly
edge1 = 4.5
edge2 = 5
# fitting first section
x_s1 = x[x<edge1]
y_s1 = y[x<edge1]
poly_s1 = fit(x_s1, y_s1)
print(poly_s1)
yfit_s1 = lambda x: np.exp(poly_s1(np.log(x_s1)))
# fitting second section
x_s2 = x[x>=edge2]
y_s2 = y[x>=edge2]
poly_s2 = fit(x_s2, y_s2)
print(poly_s2)
yfit_s2 = lambda x: np.exp(poly_s2(np.log(x_s2)))

fig = plt.figure(num=1, figsize=(6, 3.5), dpi=100, facecolor='w', edgecolor='k')
plt.loglog(x, y*1e12, 'o', color="#ffa500")
plt.plot(x_s1, yfit_s1(x_s1)*1e12, color="#1e76b3")
plt.text(1.3, 6.7, '$I\\approx V^{0.5}$', fontdict=font)
plt.plot(x_s2, yfit_s2(x_s2)*1e12, color="#1e76b3")
plt.text(10.9, 141.6, '$I\\approx V^6$', fontdict=font)
plt.ylabel('$I$ (pA)')
plt.xlabel('$V$ (V)')
#plt.show()
plt.tight_layout()
plt.savefig('out/nm_1' + '.pdf', dpi=300)
plt.savefig('out/nm_1' + '.pgf', dpi=300)
